import {HomepageAppearancePanelComponent} from './homepage-appearance-panel/homepage-appearance-panel.component';

export const APPEARANCE_PANELS = [
    HomepageAppearancePanelComponent,
];
