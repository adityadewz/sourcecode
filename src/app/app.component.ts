import {filter} from 'rxjs/operators';
import {Component, ElementRef, OnInit, ViewEncapsulation} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {BrowserEvents} from 'common/core/services/browser-events.service';
import {AppHttpClient} from 'common/core/http/app-http-client.service';
import {Settings} from 'common/core/config/settings.service';
import cssVars from 'css-vars-ponyfill';
import {CustomHomepage} from '@common/core/pages/shared/custom-homepage.service';
import {MetaTagsService} from '@common/core/meta/meta-tags.service';
import {ChannelShowComponent} from './web-player/channels/channel-show/channel-show.component';
import {ChannelResolverService} from './admin/channels/crupdate-channel-page/channel-resolver.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
    constructor(
        private browserEvents: BrowserEvents,
        private el: ElementRef,
        private http: AppHttpClient,
        private settings: Settings,
        private router: Router,
        private customHomepage: CustomHomepage,
        private meta: MetaTagsService,
    ) {}

    ngOnInit() {
        this.browserEvents.subscribeToEvents(this.el.nativeElement);
        this.settings.setHttpClient(this.http);
        this.meta.init();

        // google analytics
        if (this.settings.get('analytics.tracking_code')) {
            this.triggerAnalyticsPageView();
        }

        // custom homepage
        this.customHomepage.select({
            menuCategories: [{
                name: 'Channels',
                route: {
                    component: ChannelShowComponent,
                    resolve: {api: ChannelResolverService},
                    data: {name: 'channel'}
                }
            }],
        });

        this.loadCssVariablesPolyfill();
    }

    private triggerAnalyticsPageView() {
        this.router.events
            .pipe(filter(e => e instanceof NavigationEnd))
            .subscribe((event: NavigationEnd) => {
                if ( ! window['ga']) return;
                window['ga']('set', 'page', event.urlAfterRedirects);
                window['ga']('send', 'pageview');
            });
    }

    private loadCssVariablesPolyfill() {
        const isNativeSupport = typeof window !== 'undefined' &&
            window['CSS'] &&
            window['CSS'].supports &&
            window['CSS'].supports('(--a: 0)');
        if ( ! isNativeSupport) {
            cssVars();
        }
    }
}
