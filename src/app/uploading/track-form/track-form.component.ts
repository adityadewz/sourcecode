import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output,
    SimpleChanges
} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {BehaviorSubject} from 'rxjs';
import {CurrentUser} from '@common/auth/current-user';
import {Tracks} from '../../web-player/tracks/tracks.service';
import {Track} from '../../models/Track';
import {UploadQueueItem} from '@common/uploads/upload-queue/upload-queue-item';
import {DefaultImagePaths} from '../../web-player/default-image-paths.enum';
import {Toast} from '@common/core/ui/toast.service';
import {USER_MODEL} from '@common/core/types/models/User';
import {matExpansionAnimations, MatExpansionPanelState} from '@angular/material';
import {Modal} from '@common/core/ui/dialogs/modal.service';
import {ConfirmModalComponent} from '@common/core/ui/confirm-modal/confirm-modal.component';
import {finalize} from 'rxjs/operators';
import {openUploadWindow} from '@common/uploads/utils/open-upload-window';
import {UploadInputTypes} from '@common/uploads/upload-input-config';
import {AudioUploadValidator} from '../../web-player/audio-upload-validator';
import {UploadQueueService} from '@common/uploads/upload-queue/upload-queue.service';
import {WaveformGenerator} from '../../web-player/tracks/waveform/waveform-generator';
import {Router} from '@angular/router';
import {UploadFileResponse} from '@common/uploads/uploads-api.service';
import {Album} from '../../models/Album';
import {MixedArtist} from '../../web-player/artists/mixed-artist';
import {FileEntry} from '@common/uploads/file-entry';
import {Settings} from '@common/core/config/settings.service';
import {isAbsoluteUrl} from '@common/core/utils/is-absolute-url';

export interface ExtractedMetadata {
    title?: string;
    album?: Album;
    album_name?: string;
    artist?: MixedArtist;
    artist_name?: string;
    genres?: string[];
    duration?: number;
    release_date?: string;
    comment?: string;
    image?: FileEntry;
}

export interface TrackUploadResponse extends UploadFileResponse {
    metadata?: ExtractedMetadata;
}

@Component({
    selector: 'track-form',
    templateUrl: './track-form.component.html',
    styleUrls: ['./track-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [matExpansionAnimations.bodyExpansion]
})
export class TrackFormComponent implements OnInit, OnChanges {
    @Input() autoMatch = false;
    // track that is being edited
    @Input() track: Track;

    // creating a new track for this upload
    @Input() uploadQueueItem: UploadQueueItem;

    // track will be saved along with this album
    @Input() albumForm: FormGroup;

    // number of this track inside parent album
    @Input() number: number;

    @Output() canceled = new EventEmitter<UploadQueueItem|Track>();
    @Output() saved = new EventEmitter<Track>();

    public errors$ = new BehaviorSubject<{[K in keyof Partial<Track>]: string}>({});
    public defaultImage$ = new BehaviorSubject<string>(DefaultImagePaths.album);
    public loading$ = new BehaviorSubject<boolean>(false);

    public form = this.fb.group({
        id: [null],
        name: [''],
        image: [''],
        description: [''],
        number: [1],
        tags: [[]],
        genres: [[]],
        duration: [null],
        url: [''],
        youtube_id: [''],
        spotify_popularity: [''],
        local_only: [true],
        album: [null],
        artists: [[]],
        waveData: [null],
    });
    public expanded = false;

    constructor(
        private fb: FormBuilder,
        private currentUser: CurrentUser,
        private tracks: Tracks,
        private toast: Toast,
        private modal: Modal,
        private audioValidator: AudioUploadValidator,
        private uploadQueue: UploadQueueService,
        private waveGenerator: WaveformGenerator,
        private router: Router,
        private settings: Settings,
    ) {}

    ngOnChanges(changes: SimpleChanges) {
        if (changes.number && changes.number.currentValue != null) {
            this.form.patchValue({number: changes.number.currentValue});
        }
    }

    ngOnInit() {
        this.expanded = !this.albumForm;

        if (this.track) {
            const formValue = {...this.track};
            formValue.tags = (this.track.tags || []).map(t => t.name) as any;
            formValue.genres = (this.track.genres || []).map(t => t.display_name || t.name) as any;
            this.form.patchValue(formValue);

            if (this.track.album) {
                this.defaultImage$.next(this.track.album.image || DefaultImagePaths.album);
            }
        }

        if (this.uploadQueueItem) {
            this.uploadQueueItem.uploadedResponse$.subscribe((response: TrackUploadResponse) => {
                this.patchFormUsingFileUpload(response);
            });
        }

        if (this.albumForm) {
            this.albumForm.get('image').valueChanges.subscribe(url => {
                this.defaultImage$.next(url || DefaultImagePaths.album);
            });
        }
    }

    public getPayload(): Partial<Track> {
        const customData = this.uploadQueueItem ? this.uploadQueueItem.customData : {};
        const payload =  {...this.form.value, ...customData};
        if ( ! payload.artists.length && this.settings.get('player.artist_type') === 'user') {
            payload.artists = [{id: this.currentUser.get('id'), artist_type: USER_MODEL}];
        }
        return payload;
    }

    public isUploading() {
        return this.uploadQueueItem && !this.uploadQueueItem.completed;
    }

    public submit() {
        if (this.albumForm) return;
        this.loading$.next(true);

        const payload = this.getPayload();

        const request = this.track ?
            this.tracks.update(this.track.id, payload) :
            this.tracks.create(payload);

        request
            .pipe(finalize(() => this.loading$.next(false)))
            .subscribe(response => {
                if (this.uploadQueueItem) {
                    this.uploadQueue.remove(this.uploadQueueItem.id);
                }
                this.toast.open('Track saved.');
                this.form.markAsPristine();
                this.saved.emit(response.track);
            }, errResponse => {
                this.errors$.next(errResponse.messages);
            });
    }

    public toggleExpandedState() {
        this.expanded = !this.expanded;
    }

    public getExpandedState(): MatExpansionPanelState {
        return this.expanded ? 'expanded' : 'collapsed';
    }

    public maybeCancel() {
        this.modal.show(ConfirmModalComponent, {
            title: 'Remove Track',
            body:  'Are you sure you want to cancel the upload and remove this track?',
            ok:    'Remove'
        }).beforeClosed().subscribe(confirmed => {
            if ( ! confirmed) return;
            if (this.uploadQueueItem) {
                this.uploadQueue.remove(this.uploadQueueItem.id);
                this.canceled.emit(this.uploadQueueItem);
                this.toast.open('Upload canceled.');
            } else if (this.track) {
                this.tracks.delete([this.track.id]).subscribe(() => {
                    this.canceled.emit(this.track);
                    this.toast.open('Track deleted.');
                });
            }
        });
    }

    public openUploadMusicModal() {
        const params = {uri: 'music/upload', validator: this.audioValidator, httpParams: {autoMatch: this.autoMatch}};
        openUploadWindow({types: [UploadInputTypes.audio, UploadInputTypes.video]}).then(uploadedFiles => {
            if ( ! uploadedFiles) return;
            this.uploadQueue.start(uploadedFiles, params).subscribe(response => {
                const queueItem = this.uploadQueue.find(response.queueItemId);
                this.waveGenerator.generate(queueItem.uploadedFile.native).then(waveData => {
                    this.form.patchValue({waveData});
                });
                this.patchFormUsingFileUpload(response);
                this.toast.open('Track uploaded.');
            }, () => this.toast.open('Could not upload track'));
        });
    }

    private patchFormUsingFileUpload(response: TrackUploadResponse) {
        const values: {[K in keyof Partial<Track>]: any} = {
            name: response.metadata.title,
            duration: response.metadata.duration,
            url: response.fileEntry.url,
            genres: response.metadata.genres || [],
            description: response.metadata.comment,
        };
        if (response.metadata.album) {
            values.album = response.metadata.album;
        }
        if (response.metadata.artist) {
            values.artists = [response.metadata.artist];

            // set artist on album, if does not already have one
            if (this.albumForm && ! this.albumForm.value.artist) {
                this.albumForm.patchValue({artist: response.metadata.artist});
            }
        }
        if (response.metadata.image) {
            values.image = response.metadata.image.url;

            // set image on album, if does not already have one
            if (this.albumForm && ! this.albumForm.value.image) {
                this.albumForm.patchValue({image: response.metadata.image.url});
            }
        }
        if (response.metadata.release_date && this.albumForm && !this.albumForm.value.release_date) {
            this.albumForm.patchValue({release_date: response.metadata.release_date});
        }
        this.form.patchValue(values);
    }

    public insideAdmin(): boolean {
        return this.router.url.indexOf('admin') > -1;
    }

    public shouldShowDurationField() {
        const trackUrl = this.form.get('url').value;
        return !trackUrl || isAbsoluteUrl(trackUrl);
    }
}
