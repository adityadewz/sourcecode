import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {UploadQueueItem} from '@common/uploads/upload-queue/upload-queue-item';

@Component({
    selector: 'track-upload-header',
    templateUrl: './track-upload-header.component.html',
    styleUrls: ['./track-upload-header.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TrackUploadHeaderComponent implements OnInit {
    @Input() upload: UploadQueueItem;

    constructor() {
    }

    ngOnInit() {
    }

}
