import {UploadPageComponent} from './upload-page/upload-page.component';
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

const routes: Routes = [
    {
        path: '',
        component: UploadPageComponent,
        data: {permissions: ['tracks.create', 'albums.create']}
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UploadingRoutingModule {
}