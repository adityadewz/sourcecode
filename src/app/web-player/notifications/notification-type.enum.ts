export enum NotificationType {
    'commentReply' = 'App\\Notifications\\CommentReceivedReply',
    'newUpload' = 'App\\Notifications\\ArtistUploadedMedia'
}
