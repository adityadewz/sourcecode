export interface Notification {
    id: string;
    type: string;
    data: {[key: string]: any};
    read_at: string;
    created_at: string;
    relative_created_at: string;
}
