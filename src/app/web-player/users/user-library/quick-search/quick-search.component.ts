import { Component, NgZone, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { PaginatedDataTableSource } from "@common/shared/data-table/data/paginated-data-table-source";
import { Paginator } from "@common/shared/paginator.service";
import { WebPlayerUrls } from "../../../web-player-urls.service";
import { WebPlayerState } from "../../../web-player-state.service";
import { InfiniteScroll } from "@common/core/ui/infinite-scroll/infinite.scroll";
import { Track } from "../../../../models/Track";
import { CurrentUser } from "@common/auth/current-user";
import { queueId } from "../../../player/queue-id";
import { User } from "@common/core/types/models/User";
import { MatSort } from "@angular/material";
import { HttpClient } from "@angular/common/http";

@Component({
    selector: "quick-search",
    templateUrl: "./quick-search.component.html",
    styleUrls: ["./quick-search.component.scss"],
    providers: [Paginator]
})
export class QuickSearchComponent {
    @ViewChild(MatSort, { static: true }) matSort: MatSort;
    query = { text: null };
    results = [];
    displayedColumns: string[] = [
        // "no",
        "name",
        "label",
        "duration",
        "information"
    ];
    data = [];

    constructor(private http: HttpClient) {}
    el;
    ngOnInit() {
        this.http.get("secure/user/library/tracks").subscribe((res: any) => {
            this.data = res.pagination;
            this.results = res.pagination;
        });
    }

    enterText(text) {
        if (!text) {
            text = "";
        }
        text = text.toLowerCase();
        this.results = this.data.filter(i => {
            i.name = i.name.toLowerCase();
            return i.name.includes(text);
        });
    }

    ngOnDestroy() {}
}
