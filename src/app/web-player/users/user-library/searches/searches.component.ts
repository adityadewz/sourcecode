import { Component, NgZone, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { PaginatedDataTableSource } from "@common/shared/data-table/data/paginated-data-table-source";
import { Paginator } from "@common/shared/paginator.service";
import { WebPlayerUrls } from "../../../web-player-urls.service";
import { WebPlayerState } from "../../../web-player-state.service";
import { InfiniteScroll } from "@common/core/ui/infinite-scroll/infinite.scroll";
import { Track } from "../../../../models/Track";
import { CurrentUser } from "@common/auth/current-user";
import { queueId } from "../../../player/queue-id";
import { User } from "@common/core/types/models/User";
import { MatSort } from "@angular/material";
import { HttpClient } from "@angular/common/http";
import { COMMA, ENTER } from "@angular/cdk/keycodes";

@Component({
    selector: "searches",
    templateUrl: "./searches.component.html",
    styleUrls: ["./searches.component.scss"],
    providers: [Paginator]
})
export class SearchesComponent {
    @ViewChild(MatSort, { static: true }) matSort: MatSort;
    query = { genre: "", tempo: "", bpm: "", instruments: [], keyword: [] };
    results = [];
    filterData = {
        genre: [],
        mood: [],
        keyword: [],
        instruments: [],
        tempo: []
    };
    separatorKeysCodes: number[] = [ENTER, COMMA];

    displayedColumns: string[] = [
        // "no",
        "name",
        "label",
        "duration",
        "information"
    ];
    data = [];

    constructor(private http: HttpClient) {}
    el;

    addKeyword(k) {
        this.query.keyword.push(k);
        this.filter();
    }
    removeKeyword(i) {
        this.query.keyword.splice(i, 1);
        this.filter();
    }

    filter() {
        this.results = this.data.filter(track => {
            var has = {
                genre: this.query.genre
                    ? track.genre.includes(this.query.genre)
                    : true,
                tempo: this.query.tempo
                    ? track.tempo.includes(this.query.tempo)
                    : true,
                bpm: this.query.bpm ? track.bpm === this.query.bpm : true,
                instrument: this.query.instruments.length ? false : true,
                keyword: this.query.keyword.length ? false : true
            };
            this.query.instruments.forEach(i => {
                if (track.instruments.includes(i)) {
                    has.instrument = true;
                    return;
                }
            });
            this.query.keyword.forEach(i => {
                if (track.keyword.toLowerCase().includes(i.toLowerCase())) {
                    has.keyword = true;
                    return;
                }
            });
            return (
                has.genre &&
                has.bpm &&
                has.instrument &&
                has.tempo &&
                has.keyword
            );
        });
    }
    ngOnInit() {
        this.http.get("secure/user/library/tracks").subscribe((res: any) => {
            var GENRE = [];
            var TEMPO = [];
            var KEYWORD = [];
            var INSTRUMENTS = [];
            this.data = res.pagination;
            this.results = res.pagination;
            this.data.forEach(track => {
                if (track.genre) {
                    var genres = track.genre.split(",");
                    genres.forEach(g => {
                        if (GENRE.indexOf(g) === -1) {
                            GENRE.push(g);
                        }
                    });
                }
                if (track.keyword) {
                    var keywords = track.keyword.split(",");
                    keywords.forEach(g => {
                        if (KEYWORD.indexOf(g) === -1) {
                            KEYWORD.push(g);
                        }
                    });
                }
                if (track.tempo) {
                    var tempos = track.tempo.split(",");
                    tempos.forEach(g => {
                        if (TEMPO.indexOf(g) === -1) {
                            TEMPO.push(g);
                        }
                    });
                }
                if (track.instruments) {
                    var instrumentss = track.instruments.split(",");
                    instrumentss.forEach(g => {
                        if (INSTRUMENTS.indexOf(g) === -1) {
                            INSTRUMENTS.push(g);
                        }
                    });
                }
            });
            this.filterData.genre = GENRE;
            this.filterData.tempo = TEMPO;
            this.filterData.instruments = INSTRUMENTS;
            this.filterData.keyword = KEYWORD;
        });
    }

    enterText(text) {
        if (!text) {
            text = "";
        }
        text = text.toLowerCase();
        this.results = this.data.filter(i => {
            i.name = i.name.toLowerCase();
            return i.name.includes(text);
        });
    }

    ngOnDestroy() {}
}
