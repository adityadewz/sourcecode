import {Component, Input} from '@angular/core';
import {Track} from '../../../../../models/Track';
import {BaseProfileTab} from '../base-profile-tab';
import {ProfileTabFadeAnimation} from '../profile-tab-fade-animation';

@Component({
    selector: 'uploaded-tracks-tab',
    templateUrl: './uploaded-tracks-tab.component.html',
    styleUrls: ['./uploaded-tracks-tab.component.scss'],
    animations: [ProfileTabFadeAnimation],
})
export class UploadedTracksTabComponent extends BaseProfileTab<Track> {
    @Input() contentType: string;
}
