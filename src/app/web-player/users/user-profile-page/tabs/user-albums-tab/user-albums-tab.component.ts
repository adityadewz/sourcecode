import {ChangeDetectionStrategy, Component} from '@angular/core';
import {BaseProfileTab} from '../base-profile-tab';
import {Album} from '../../../../../models/Album';
import {ProfileTabFadeAnimation} from '../profile-tab-fade-animation';

@Component({
    selector: 'user-albums-tab',
    templateUrl: './user-albums-tab.component.html',
    styleUrls: ['./user-albums-tab.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [ProfileTabFadeAnimation],
})
export class UserAlbumsTabComponent extends BaseProfileTab<Album> {
    protected contentType = 'albums';
}
