import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserProfilePageComponent} from './user-profile-page.component';
import {EditUserProfileModalComponent} from './edit-user-profile-modal/edit-user-profile-modal.component';
import {UploadedTracksTabComponent} from './tabs/uploaded-tracks-tab/uploaded-tracks-tab.component';
import {UserAlbumsTabComponent} from './tabs/user-albums-tab/user-albums-tab.component';
import {UserPlaylistsTabComponent} from './tabs/user-playlists-tab/user-playlists-tab.component';
import {UserRepostsTabComponent} from './tabs/user-reposts-tab/user-reposts-tab.component';
import {UserFollowsTabComponent} from './tabs/user-follows-tab/user-follows-tab.component';
import {UiModule} from '@common/core/ui/ui.module';
import {MediaImageModule} from '../../shared/media-image/media-image.module';
import {RouterModule} from '@angular/router';
import {MatDialogModule} from '@angular/material';
import {UploadImageControlModule} from '@common/shared/form-controls/upload-image-control/upload-image-control.module';
import {MediaListItemModule} from '../../tracks/media-list-item/media-list-item.module';
import {PlayerUiModule} from '../../player-ui.module';
import {TrackActionsBarModule} from '../../tracks/track-actions-bar/track-actions-bar.module';
import {UserProfileRoutingModule} from './user-profile-routing.module';

@NgModule({
    declarations: [
        UserProfilePageComponent,
        EditUserProfileModalComponent,
        UploadedTracksTabComponent,
        UserAlbumsTabComponent,
        UserPlaylistsTabComponent,
        UserRepostsTabComponent,
        UserFollowsTabComponent,
    ],
    imports: [
        CommonModule,
        UiModule,
        UploadImageControlModule,
        MediaImageModule,
        MediaListItemModule,
        PlayerUiModule,
        TrackActionsBarModule,
        RouterModule,
        UserProfileRoutingModule,

        MatDialogModule,
    ],
    entryComponents: [
        EditUserProfileModalComponent,
    ]
})
export class UserProfileModule {
}
