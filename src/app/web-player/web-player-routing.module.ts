import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AlbumComponent } from "./albums/album/album.component";
import { AlbumResolver } from "./albums/album/album-resolver.service";
import { LibraryTracksComponent } from "./users/user-library/library-tracks/library-tracks.component";
import { LibraryAlbumsComponent } from "./users/user-library/library-albums/library-albums.component";
import { LibraryArtistsComponent } from "./users/user-library/library-artists/library-artists.component";
import { ArtistComponent } from "./artists/artist/artist.component";
import { ArtistResolver } from "./artists/artist/artist-resolver.service";
import { GenreComponent } from "./genres/genre/genre.component";
import { GenreArtistsResolver } from "./genres/genre/genre-artists-resolver.service";
import { PlaylistComponent } from "./playlists/playlist/playlist.component";
import { PlaylistResolver } from "./playlists/playlist/playlist-resolver.service";
import { SearchComponent } from "./search/search/search.component";
import { SearchResolver } from "./search/search/search-resolver.service";
import { SearchTabValidGuard } from "./search/search/search-tab-valid.guard";
import { TrackPageComponent } from "./tracks/track-page/track-page.component";
import { TrackPageResolver } from "./tracks/track-page/track-page-resolver.service";
import { RadioPageComponent } from "./radio-page/radio-page.component";
import { RadioPageResolver } from "./radio-page/radio-page-resolver.service";
import { UserLibraryComponent } from "./users/user-library/user-library.component";
import { LibraryPlaylistsComponent } from "./users/user-library/library-playlists/library-playlists.component";
import { EmptyRouteComponent } from "common/core/ui/empty-route/empty-route.component";
import { CheckPermissionsGuard } from "common/guards/check-permissions-guard.service";
import { AuthGuard } from "common/guards/auth-guard.service";
import { AccountSettingsResolve } from "common/account-settings/account-settings-resolve.service";
import { AccountSettingsComponent } from "common/account-settings/account-settings.component";
import { CustomPageComponent } from "@common/core/pages/shared/custom-page/custom-page.component";
import { ChannelShowComponent } from "./channels/channel-show/channel-show.component";
import { ChannelResolverService } from "../admin/channels/crupdate-channel-page/channel-resolver.service";
import { PlayHistoryComponent } from "./users/user-library/play-history/play-history.component";
import { TrackEmbedComponent } from "./tracks/track-embed/track-embed.component";
import { NotificationsPageComponent } from "./notifications/notifications-page/notifications-page.component";
import { NotificationsResolverService } from "./notifications/notifications-resolver.service";
import { ChannelFallbackHostComponent } from "./channels/channel-fallback-host/channel-fallback-host.component";
import { HomepageHostComponent } from "./homepage-host/homepage-host.component";
import { NOT_FOUND_ROUTES } from "@common/core/pages/not-found-routes";
import { SearchesComponent } from "./users/user-library/searches/searches.component";
import { QuickSearchComponent } from "./users/user-library/quick-search/quick-search.component";

const routes: Routes = [
    {
        path: "track/:id/:name/embed",
        component: TrackEmbedComponent,
        resolve: { api: TrackPageResolver },
        data: { name: "embed track" }
    },
    {
        path: "album/:id/:artist/:album/embed",
        component: TrackEmbedComponent,
        resolve: { api: AlbumResolver },
        data: { name: "embed album" }
    },
    {
        path: "",
        component: HomepageHostComponent,
        canActivateChild: [CheckPermissionsGuard],
        data: { willSetSeo: true, parentHomeRoute: true },
        children: [
            {
                path: "user",
                loadChildren: () =>
                    import(
                        "app/web-player/users/user-profile-page/user-profile.module"
                    ).then(m => m.UserProfileModule)
            },
            {
                path: "upload",
                data: { permissions: ["tracks.create", "albums.create"] },
                loadChildren: () =>
                    import("app/uploading/uploading.module").then(
                        m => m.UploadingModule
                    )
            },

            {
                path: "channels/:slug",
                component: ChannelShowComponent,
                resolve: { api: ChannelResolverService },
                data: { name: "channel", failRedirectUri: "/", noReuse: true }
            },

            {
                path: "album/:id/:artist/:album",
                component: AlbumComponent,
                resolve: { api: AlbumResolver },
                data: { name: "album" }
            },

            {
                path: "genre/:name",
                component: GenreComponent,
                resolve: { api: GenreArtistsResolver },
                data: { name: "genre" }
            },

            {
                path: "playlists/:id",
                component: PlaylistComponent,
                resolve: { api: PlaylistResolver },
                data: { name: "playlist", noReuse: true }
            },
            {
                path: "playlists/:id/:name",
                component: PlaylistComponent,
                resolve: { api: PlaylistResolver },
                data: { name: "playlist", noReuse: true }
            },

            {
                path: "track/:id",
                component: TrackPageComponent,
                resolve: { api: TrackPageResolver },
                data: { name: "track" }
            },
            {
                path: "track/:id/:name",
                component: TrackPageComponent,
                resolve: { api: TrackPageResolver },
                data: { name: "track" }
            },

            {
                path: "search",
                component: SearchComponent,
                data: { name: "search" }
            },
            {
                path: "search/:query",
                component: SearchComponent,
                resolve: { results: SearchResolver },
                data: { name: "search" }
            },
            {
                path: "search/:query/:tab",
                component: SearchComponent,
                resolve: { results: SearchResolver },
                canActivate: [SearchTabValidGuard],
                data: { name: "search" }
            },

            {
                path: "artist/:id/:name",
                component: ArtistComponent,
                resolve: { api: ArtistResolver },
                data: { name: "artist", noReuse: true },
                children: [{ path: ":tabName", component: EmptyRouteComponent }]
            },

            {
                path: "radio/artist/:id/:name",
                component: RadioPageComponent,
                resolve: { radio: RadioPageResolver },
                data: { type: "artist", name: "radio" }
            },
            {
                path: "radio/track/:id/:name",
                component: RadioPageComponent,
                resolve: { radio: RadioPageResolver },
                data: { type: "track", name: "radio" }
            },

            {
                path: "library",
                component: UserLibraryComponent,
                canActivate: [AuthGuard],
                canActivateChild: [AuthGuard],
                children: [
                    { path: "", redirectTo: "songs", pathMatch: "full" },
                    {
                        path: "songs",
                        component: LibraryTracksComponent,
                        data: { name: "library.tracks", title: "Your Tracks" }
                    },
                    {
                        path: "albums",
                        component: LibraryAlbumsComponent,
                        data: { name: "library.albums", title: "Your Albums" }
                    },
                    {
                        path: "artists",
                        component: LibraryArtistsComponent,
                        data: { name: "library.artists", title: "Your Artists" }
                    },
                    {
                        path: "playlists",
                        component: LibraryPlaylistsComponent,
                        data: {
                            name: "library.playlists",
                            title: "Your Playlists"
                        }
                    },
                    {
                        path: "history",
                        component: PlayHistoryComponent,
                        data: {
                            name: "library.history",
                            title: "Listening History"
                        }
                    },
                    {
                        path: "advanced-search",
                        component: SearchesComponent,
                        data: {
                            name: "library.search",
                            title: "Listening Search"
                        }
                    },
                    {
                        path: "quick-search",
                        component: QuickSearchComponent,
                        data: {
                            name: "library.search",
                            title: "Listening Search"
                        }
                    },
                    {
                        path: "**",
                        redirectTo: "/library/quick-search"
                    }
                ]
            },

            { path: "account-settings", redirectTo: "account/settings" },
            {
                path: "account/settings",
                component: AccountSettingsComponent,
                resolve: { api: AccountSettingsResolve },
                canActivate: [AuthGuard],
                data: { name: "account-settings" }
            },

            {
                path: "account/notifications",
                component: NotificationsPageComponent,
                resolve: { api: NotificationsResolverService },
                canActivate: [AuthGuard],
                data: { name: "notifications" }
            },

            {
                path: "pages/:id/:slug",
                component: CustomPageComponent,
                data: { permissions: ["pages.view"], willSetSeo: true }
            },

            {
                path: ":slug",
                resolve: { api: ChannelResolverService },
                component: ChannelFallbackHostComponent,
                data: { noReuse: true }
            },

            ...NOT_FOUND_ROUTES
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class WebPlayerRoutingModule {}
